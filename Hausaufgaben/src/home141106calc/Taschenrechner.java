package home141106calc;

public class Taschenrechner {
	private Display display;
	private double zahl1; // erste zahl vorkomma
	private double zahl2; // zweite zahl vorkomma
	private double zahl3; // erste nachkommazahl --> zahl1 = zahl1 + zahl3
	private double zahl4; // zweite nachkommazahl
	// operationszeichen setzen für ausgabe und berechnung
	private char operation;
	// Zustandsvar um nach best eingabe die zustände zu ändern
	private char zustand = '1';
	private double erg; // erg aus zahl1(,zahl3) und zahl2(,zahl4)
	//zaehler fuer die anzahl der nachkommastellen (zahl3/zahl4)
	private int zaehler=0;
	public void verarbeite(char zeichen) {
		switch (zustand) {
		// zustand für eingabe 1.zahl
		case '1':
			// Eingabe abfragen
			switch (zeichen) {
			case 'c': //
				if (zahl1 != 0)
					display.ausgabe(zahl1);
				zahl1 = 0;
				zahl2 = 0;
				erg = 0;
				System.out.println("\nC gedrückt - Display leer");
				zustand = 1;

				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				zahl1 = (zahl1 * 10) + zeichen - '0';
				display.ausgabe(zeichen - '0');
				break;
			case ',':
				zustand = '4'; // nachkommastelle zahl1!!
				display.ausgabe('.');
				
				break;
			case '+':
				operation = '+';
				zustand = '2'; // zustand auf plus --> eingabe zahl2
				display.ausgabe(operation);
				break;
			case '-':
				operation = '-';
				zustand = '2'; // zustand auf minus --> eingabe zahl2
				display.ausgabe(operation);
				break;
			case '*':
				operation = '*';
				zustand = '2'; // zustand auf mal --> eingabe zahl2
				display.ausgabe(operation);
				break;
			case '/':
				operation = '/';
				zustand = '2'; // zustand auf div --> eingabe zahl2
				display.ausgabe(operation);
				break;
			case '=':
				System.out.print(" = ");
				display.ausgabe(zahl1);
				zustand = '3';
			}
			break;
		case '2': // eingabe 2.zahl wenn operation + - * / gedrückt wurde
			switch (zeichen) {
			case 'c':
				if (zahl2 != 0)
					display.ausgabe(zahl2);
				zahl1 = 0;
				zahl2 = 0;
				erg = 0;
				System.out.println("\nC gedrückt - Display leer");
				zustand = 1;

				break;

			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				zahl2 = (zahl2 * 10) + zeichen - '0';
				display.ausgabe(zeichen - '0');
				
				break;
			case ',':
				zustand = '5'; // nachkommastelle zahl1!!
				display.ausgabe('.');
				break;
			case '=':
				if (operation == '+') {
					erg = zahl2 + zahl1;
				} else if (operation == '-') {
					erg = zahl1 - zahl2;
				} else if (operation == '*') {
					erg = zahl1 * zahl2;
				} else if (operation == '/' & zahl2 != '0') {
					erg = zahl1 / zahl2;
				}
				// display.ausgabe(zahl2);
				System.out.print(" = ");

				display.ausgabe(erg);

				zustand = '3';

				break;
			}
			break;
		case '3':

			zahl2 = 0;
			zahl1 = erg;
			switch (zeichen) {
			case 'c':
				if (zahl2 != 0)
					display.ausgabe(zahl2);
				zahl1 = 0;
				zahl2 = 0;
				erg = 0;
				System.out.println("\nC gedrückt - Display leer");
				zustand = 1;
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				erg = 0; // testweise
				zahl1 = zeichen - '0';
				// display.ausgabe(zahl1);
				// System.out.print("\n");
				display.ausgabe('c');
				zustand = '1';
				break;
			case '+':
				operation = '+';
				zustand = '2'; // zustand auf plus --> eingabe zahl2
				// display.ausgabe((int) zahl1);
				display.ausgabe(operation);

				break;
			case '-':
				operation = '-';
				zustand = '2'; // zustand auf minus --> eingabe zahl2
				// display.ausgabe((int) zahl1);
				display.ausgabe(operation);
				break;
			case '*':
				operation = '*';
				zustand = '2'; // zustand auf mal --> eingabe zahl2
				// display.ausgabe((int) zahl1);
				display.ausgabe(operation);
				break;
			case '/':
				operation = '/';
				zustand = '2'; // zustand auf div --> eingabe zahl2
				// display.ausgabe((int) zahl1);
				display.ausgabe(operation);
				break;

			}
			break;
		case '4':

			switch (zeichen) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				zahl3 = (zahl3 * 10) + zeichen - '0';
				display.ausgabe(zeichen - '0');
				//zaehler++;
				break;
			case '=':
				zustand = '3';
				zahl3 = this.afterComma(zahl3);
				zahl1 = zahl1 + zahl3;
				display.ausgabe(zahl1);
				break;
			case '+':
				operation = '+';
				zustand = '2';
				zahl3 = this.afterComma(zahl3);
				zahl1 = zahl1 + zahl3;
				display.ausgabe(operation);
				break;
			case '-':
				operation = '-';
				zustand = '2';
				zahl3 = this.afterComma(zahl3);
				zahl1 = zahl1 + zahl3;
				display.ausgabe(operation);
				break;
			case '*':
				operation = '*';
				zustand = '2';
				zahl3 = this.afterComma(zahl3);
				zahl1 = zahl1 + zahl3;
				display.ausgabe(operation);
				break;
			case '/':
				operation = '/';
				zustand = '2';
				zahl3 = this.afterComma(zahl3);
				zahl1 = zahl1 + zahl3;
				display.ausgabe(operation);
				break;
			}
			break;
		case '5':

			switch (zeichen) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				zahl4 = (zahl4 * 10) + zeichen - '0';
				display.ausgabe(zeichen - '0');
				//zaehler++;
				break;
			case '+':
				operation = '+';
				zustand = '2';
				zahl4 = this.afterComma(zahl4);
				zahl2 = zahl2 + zahl4;
				display.ausgabe(operation);
				break;
			case '-':
				operation = '-';
				zustand = '2';
				zahl4 = this.afterComma(zahl4);
				zahl2 = zahl2 + zahl4;
				display.ausgabe(operation);
				break;
			case '*':
				operation = '*';
				zustand = '2';
				zahl4 = this.afterComma(zahl4);
				zahl2 = zahl2 + zahl4;
				display.ausgabe(operation);
				break;
			case '/':
				operation = '/';
				zustand = '2';
				zahl4 = this.afterComma(zahl4);
				zahl2 = zahl2 + zahl4;
				display.ausgabe(operation);
				break;
			case '=':
				zahl4 = this.afterComma(zahl4);
				zahl2 = zahl2 + zahl4;
				if (operation == '+') {
					erg = zahl2 + zahl1;
				} else if (operation == '-') {
					erg = zahl1 - zahl2;
				} else if (operation == '*') {
					erg = zahl1 * zahl2;
				} else if (operation == '/' & zahl2 != '0') {
					erg = zahl1 / zahl2;
				}
				System.out.print(" = ");
				display.ausgabe(erg);
				zustand = '3';
				break;
			}
			break;
		}
	}

	public void setDisplay(Display display) {
		this.display = display;
	}

//	 private double afterComma1(int zaehler){
//	 double comma=0;
//	 String comma1 = "";
//	 char ziffer = ' ';
//	 zaehler = (char)zaehler;
//	 comma1 = comma1+ziffer;
//	 return comma;
//	 }

	private double afterComma(double zahl) {
		int hilfe = 1;
		double comma = 0;
		for (int i = 1; i <= (int) Math.log10(zahl) + 1; i++) {
			hilfe = hilfe * 10;
		}
		zahl = zahl / hilfe;
		comma = zahl;
		return comma;
	}

}
