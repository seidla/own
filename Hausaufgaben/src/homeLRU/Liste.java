package homeLRU;
import homeRouter.Netzwerk;

import java.util.Iterator;


public class Liste implements Iterable{
	
	
	private Element first;
	
	public Iterator iterator(){
		return new Counter();
	}
	
	private class Counter implements Iterator{
		private Element current=first;
		
		public boolean hasNext(){
			return (current!=null);
		}
		
		public Object next(){
			Object ret = current.value;
			current=current.next;
			return ret;
		}

		public void remove() {
			
			
		}
	}
	
	private static class Element {
		private Object value;
		private Element next;
		
		public Element(Object value) {
			this.value = value;
		}
	}
	
	// Pr�fen, ob die Liste leer ist
	public Boolean isEmpty() {
		return (first == null); 
	}
	
	// Anhängen an letzten Wagen
	public void addEnd(Object value) {
		if (first == null)
			first = new Element(value);
		else {
			Element e = first;
			for (; e.next!= null; e = e.next);
				e.next=new Element(value);
		}
	}
	
	// Element anh�ngen
	public void add(Object value) {

		if (isEmpty()) {
			first = new Element(value);
		} else {
			Element neu = new Element(value);
			neu.next=first;
			first = neu;
		}
	}

	// Gr��e der Liste ermitteln
	public int getSize() {

		int size = 0;
		Element ele = first;

		while (ele != null) {
			size++;
			ele = ele.next;
		}

		return size;
	}

	// Liste ausgeben
	public String toString() {

		String ausgabe = "";

		Element ele = first;
		while (ele != null) {
			ausgabe += "***" + ele.value;
			ele = ele.next;
		}

		return ausgabe;
	}

	// Element l�schen
	public void delete(Object value) {
		Element ele, last = first;

		// Liste leer
		if (first == null)
			return;

		// Erstes Element matcht
		if (first.next.equals(value)) {
			first = first.next;
			return;
		}

		// die weiteren Elemente werden untersucht
		ele = last.next;

		while (ele != null) {
			if (ele.next.equals(value)) {
				last.next=ele.next;
				break; // hier abbrechen, da Element gefunden wurde
			}

			last = ele;
			ele = ele.next;
		}
	}

	// ********************************************************************************************************

	// Element hinten anh�ngen
	public void push(Object ele) {
		add(ele);
	}

	// letztes Element l�schen und zur�ckliefern
	public Object pop() {

		if (isEmpty())
			return null;

		Element ele = first;

		first = first.next;

		return ele.value;
	}

	// Index des Elements ermitteln
	public int getIndex(String text) {

		int index = getSize() - 1;
		Element ele = first;

		while (ele != null) {
			if (ele.next.equals(text)) {
				break;
			}
			ele = ele.next;
			index--;
		}

		return index;
	}

	// Wert von einem bestimmten Index zur�ckliefern
	public Object atIndex(int index) {

		Object text = null;
		int counter = getSize() - 1;
		Element ele = first;

		while (ele != null) {
			if (counter == index) {
				text = ele.value;
				break;
			}
			ele = ele.next;
			counter--;
		}

		return text;
	}

	// ********************************************************************************************************
	public Object removeFirst() {
		Object o = first.value;
		first = first.next;
		return o;
	}
	// entfernt einen Wert beim gegebenen Index
	public Object remove(int index) {

		Object value = null;
		int lastIndex = getSize() - 1;

		// Liste leer oder Index liegt au�erhalb
		if (isEmpty() || index < 0 || index > lastIndex) {
			return value;
		}

		// Index ist das erste Element in der Liste
		if (index == lastIndex) {
			value = first.value;
			first = first.next;
			return value;
		}

		// nachfolgende Elemente anschauen und den Index dabei mitz�hlen
		Element ele = first.next;
		Element pre = first;
		int counter = 1;

		while (ele != null) {

			if (index == lastIndex - counter) {
				value = first.value;
				pre.next=ele.next;
				break;
			}

			pre = ele;
			ele = ele.next;
			counter++;
		}

		return value;
	}

	// entfernt einen Wert beim gegebenen Index
	public void insert(int index, String text) {

		int lastIndex = getSize() - 1;

		// Liste leer oder Index liegt au�erhalb
		if (isEmpty() || index < 0 || index > lastIndex + 1) {
			return;
		}

		Element newEle = new Element(text);
		
		// Element wird als letztes eingef�gt
		if (index == lastIndex + 1) {
			newEle.next=first;
			first = newEle;
			return;
		}

		// Element wird als vorletztes eingef�gt
		if (index == lastIndex) {
			newEle.next=first.next;
			first.next=newEle;
			return;
		}

		// nachfolgende Elemente anschauen und den Index dabei mitz�hlen
		Element ele = first.next;
		int counter = 1;

		while (ele != null) {

			if (lastIndex - counter == index) {
				newEle.next=first.next;
				ele.next=newEle;
				break;
			}

			ele = ele.next;
			counter++;

		}

	}
	

}
