package homeLRU;

public class TestSystem {
	public static void main(String[] args) {

		Liste main = new Liste(); // Frames
		int frameSize = 3; // Frame size

		Prozess pr = new Prozess();
		int pageCount = 0;
		Liste pag = new Liste();
		for (Page p : pr.work()) {
			pag.add(p);
			System.out.println(p.toString() + " " + p.getR());
		}
		int pageSize = pag.getSize();
		System.out.println();
		if (pageSize >= frameSize)
			for (; pageCount < frameSize; pageCount++) {

				Page pg = (Page) pag.removeFirst();
				main.add(pg);
				System.out.println(pg.toString() + " " + pg.getR());
			}
		else
			for (; pageCount < pageSize; pageCount++) {

				Page pg = (Page) pag.removeFirst();
				main.add(pg);
				System.out.println(pg.toString() + " " + pg.getR());
			}
		System.out.println();

		for (; frameSize > 0; frameSize--) {
			Page pe = (Page) main.removeFirst();
			pe.setR(false);
			main.addEnd(pe);
		}

		System.out.println();
		for (Object o : main) {
			Page pf = (Page) o;
			System.out.println(pf.toString() + " " + pf.getR());

		}
	}
}
