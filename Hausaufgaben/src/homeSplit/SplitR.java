package homeSplit;

public class SplitR {

	public static void main(String[] args) {
		String alt = "Hallo schöne Welt!";
		// String alt1 =
		// "Fallschirmjäger sein, ist der Protest gegen das Gesetz des Durchschnitts!";
		String[] neu = split(alt, ' ');

		for (String s : neu)
			System.out.println(s);
	}

	// Ver 0.1 StrArr mit Länge von Str
	public static String[] split1(String s, char c) {
		String[] ret = new String[s.length() / 2];
		String aktWert = "";
		int indArr = 0;
		int indStr = 0;
		if (s.length() == 0 || s.indexOf(c) == -1)
			return null;
		return split1(s, c, ret, indArr, indStr, aktWert);
	}

	private static String[] split1(String alt, char split, String[] ret,int indArr, int indStr, String aktWert) {
		String[] tmp = new String[ret.length + 1];
		if (indStr == alt.length()) {
			ret[indArr] = aktWert;
			return ret;
		}
		if (alt.charAt(indStr) == split) {
			ret[indArr] = aktWert;
			aktWert = "";
			indStr++;
			indArr++;
		} else {
			aktWert += alt.charAt(indStr);
			indStr++;
		}
		return split1(alt, split, ret, indArr, indStr, aktWert);
	}

	// Ver 0.2
	public static String[] split0(String text, char a) {
		if (text.length() == 0)
			return null;
		return split0(text, a, 0);
	}

	private static String[] split0(String text, char a, int i) {
		if (text.length() == 0)
			return new String[i + 1];
		if (text.charAt(0) == a) {
			return split0(text.substring(1), a, i + 1);
		} else {
			String[] sa = split0(text.substring(1), a, i);
			if (sa[i] == null)
				sa[i] = text.charAt(0) + "";
			else
				sa[i] = text.charAt(0) + sa[i];
			return sa;
		}
	}
	
	
	
	// Ver0.3
	public static String[] split(String s, char c) {
		if (s.length() == 0 || s.indexOf(c) == -1)
			return null;
		return split(s, c, 0);
	}

	private static String[] split(String s, char c, int i) {
		String[] ret = new String[0];
		String n = "";
		if (s.length() == 0)
			return new String[i + 1];
		if (s.indexOf(c) >= 0) {
			n = s.substring(0, s.indexOf(c));
			s = s.substring(s.indexOf(c) + 1);
			ret = split(s, c, i + 1);
			ret[i] = n;
			return ret;
		} else {
			n = s.substring(0);
			s = "";
			ret = split(s, c, i);
			ret[i] = n;
			return ret;
		}
	}
}
