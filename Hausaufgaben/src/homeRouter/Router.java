package homeRouter;

public class Router {
	private Liste rTable = new Liste();
	

	public void addRoute(Route rt) {
		rTable.add(rt);
	}

	public String bestMatch(IPAdresse ip){
		Route best = rTable.atIndex(3);
		
		for(int i=rTable.size()-1;i>=0;i--){
			if(rTable.atIndex(i).lpm(ip)>best.lpm(ip))
					best=rTable.atIndex(i);
		}
		if(best.lpm(ip)<0)
			return "Keine Route verfügbar";
		else
			return "über " + best.getIface()+" an " +best.getGw();
	}
	
	public String toString(){
		String out="";
		System.out.println("NetzwerkIP"+'\t'+"Subnetzmaske"+'\t'+"Gateway"+'\t'+"Interface");
		for(int i = rTable.size()-1;i>=0;i--)
			out+=rTable.atIndex(i).toString()+'\n';
		return out;
	}
}
