package homeRouter;

public class Route {
	private Netzwerk nw;
	private IPAdresse gw;
	private int iface;

	
	public Route(Netzwerk nw,IPAdresse gw,int iface){
		this.nw=nw;
		this.gw=gw;
		this.iface=iface;
	}

	public Netzwerk getNw() {
		return nw;
	}

	public IPAdresse getGw() {
		return gw;
	}

	public int getIface() {
		return iface;
	}

	public int lpm(IPAdresse ip){
		if(nw.match(ip))
			return nw.getMaskLength();
		else
			return -1;
	}
	
	@Override
	public String toString() {
		return nw.getNetzwerkIP().toString()+"/"+nw.getMaskLength()+'\t'+nw.getMask().toString()+'\t'+gw.toString()+'\t'+iface;
	}

	
}
