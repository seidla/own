package homeRouter;


public class Netzwerk {

	private IPAdresse netzID;
	private IPAdresse maske;
	
	public Netzwerk(IPAdresse ip) {
		netzID = ip;
		if (ip.get(1) < 128)
			maske = new IPAdresse(255,0,0,0);
		else if (ip.get(1) < 192)
			maske = new IPAdresse(255,255,0,0);
		else if (ip.get(1) < 224)
			maske = new IPAdresse(255,255,255,0);
		else
			maske = new IPAdresse(255,255,255,255);
	}
	
	public Netzwerk(IPAdresse ip, IPAdresse maske) {
		netzID = ip;
		this.maske = maske;
	}
	
	public IPAdresse getNetzwerkIP() {
		return new IPAdresse(netzID.toInt() & maske.toInt());
	}
	
	public IPAdresse getMask() {
		return maske;
	}
	
	public int getMaskLength(){
		return 32-Integer.numberOfTrailingZeros(maske.toInt());
	}
	public boolean match (IPAdresse ip){
		return ((ip.toInt()&this.maske.toInt())==getNetzwerkIP().toInt());
	}
	
	public IPAdresse getBroadcast() {
		return new IPAdresse(netzID.toInt() | ~maske.toInt());
	}
	
	public int getMaxClients() {
		return (~maske.toInt())-1;
	}
	
	public Netzwerk[] split() {
		int neueMaske = maske.toInt() >> 1;
		return new Netzwerk[] {
			new Netzwerk(getNetzwerkIP(),new IPAdresse(neueMaske)),
			new Netzwerk(new IPAdresse(getBroadcast().toInt() & neueMaske),new IPAdresse(neueMaske))
		};
	}
	
	public String toString() {
		return getNetzwerkIP()+"/"+maske;
	}
}
