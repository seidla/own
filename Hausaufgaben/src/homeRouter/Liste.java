package homeRouter;

public class Liste {

	private Element first;
	
	
	public boolean isEmpty() {
		return first == null;
	}
	public void add(Netzwerk nw) {
		Element neu = new Element(nw);
		if (first == null)
			first = neu;
		else {
			Element tmp=first;
			while (tmp.getNext() != null) tmp = tmp.getNext();
			tmp.setNext(neu);
		}
	}
	
	public void add(Route rt) {
		Element neu = new Element(rt);
		if (first == null)
			first = neu;
		else {
			Element tmp=first;
			while (tmp.getNext() != null) tmp = tmp.getNext();
			tmp.setNext(neu);
		}
	}
	
	public Route atIndex(int index) {
		if (isEmpty())
			return null;
		int count = size();
		if (index == count - 1)
			return first.getRt();
		else {
			Element e = first;
			for (int j = 0; j < count - index - 1; j++) {
				e = e.getNext();
			}
			return e.getRt();
		}
	}
	
	public Netzwerk removeFirst() {
		Netzwerk nw = first.getValue();
		first = first.getNext();
		return nw;
	}
	public int size() {
		int i = 0;
		for (Element tmp=first; tmp!=null; tmp = tmp.getNext(), i++);
		return i;
	}
}
