package homeRouter;

public class Sim {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		IPAdresse dstIP = new IPAdresse("172.168.0.5");
		Router rt = new Router();
		rt.addRoute(new Route(new Netzwerk(new IPAdresse("125.0.0.2"), new IPAdresse("255.255.0.0")), new IPAdresse("137.241.0.7"), 1));//16
		rt.addRoute(new Route(new Netzwerk(new IPAdresse("172.168.0.4"), new IPAdresse("255.255.192.0")),new IPAdresse("137.241.0.8"), 2));//18
		rt.addRoute(new Route(new Netzwerk(new IPAdresse("192.168.0.4"), new IPAdresse("255.255.255.192")),new IPAdresse("137.241.0.9"), 3));//24
		System.out.println(rt.toString());
//		System.out.println(new Netzwerk(dstIP).getMaskLength());
		
		
		System.out.println(rt.bestMatch(dstIP));
		
	
	}

}
