package homeRouter;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Subnetting {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Subnetzrechner");
		System.out.println("--------------");
		IPAdresse start = null;
		while (start == null) {
			System.out.println("Geben Sie zun�chst die IP-Adresse ein:");
			String ip = scanner.nextLine();
			String[] ipo = ip.split("\\.");
			if (ipo.length == 4)
				start = new IPAdresse( new int[] {
					Integer.parseInt(ipo[0]),
					Integer.parseInt(ipo[1]),
					Integer.parseInt(ipo[2]),
					Integer.parseInt(ipo[3])} );
			else
				System.out.println("Ung�ltige IP-Adresse! Bitte mit 3 Punkten eingeben.");
		}
		Netzwerk nw = null;
		while (nw == null) {
			System.out.println("Geben Sie jetzt die Maske ein:");
			String ip = scanner.nextLine();
			if (ip.length() == 0)
				nw = new Netzwerk(start);
			else {
				String[] ipo = ip.split("\\.");
				if (ipo.length == 4)
					nw = new Netzwerk(start, new IPAdresse( new int[] {
						Integer.parseInt(ipo[0]),
						Integer.parseInt(ipo[1]),
						Integer.parseInt(ipo[2]),
						Integer.parseInt(ipo[3])} ) );
				else if (ipo.length == 1)
					nw = new Netzwerk(start, new IPAdresse(
							(1 << 31) >> (Integer.parseInt(ipo[0])-1) ));
				else
					System.out.println("Ung�ltige Maske! Bitte mit 3 Punkten eingeben.");
			}
		}
		System.out.println("Gew�hltes Netzwerk:");
		System.out.println(" "+nw);
		System.out.println(start.get(0));
		int auswahl = 0;
		while (auswahl == 0) {
			System.out.println("Was wollen Sie machen?");
			System.out.println(" 1. Subnetze nach Anzahl Netze");
			System.out.println(" 2. Subnetze nach Anzahl Clients");
			String in = scanner.nextLine();
			if (in.trim().equals("1"))
				auswahl = 1;
			else if (in.trim().equals("2"))
				auswahl = 2;
		}
		if (auswahl == 1) {
			System.out.println("Wieviel Netze wollen Sie haben?");
			String in = scanner.nextLine();
			int anzahl = Integer.parseInt(in);
		
			Liste list = new Liste();
			list.add(nw);
			while (list.size() < anzahl) {
				Netzwerk[] neu = list.removeFirst().split();
				list.add(neu[0]);
				list.add(neu[1]);
			}
			while (!list.isEmpty())
				System.out.println(list.removeFirst());
		}
		else {
			System.out.println("Wieviel Clinets sollen in die Netze?");
			String in = scanner.nextLine();
			int anzahl = Integer.parseInt(in);
			
			Liste list = new Liste();
			list.add(nw);
			
			while ((nw = list.removeFirst()).split()[0].getMaxClients() > anzahl) {
				Liste neu = new Liste();
				Netzwerk[] sns = nw.split();
				neu.add(sns[0]);
				neu.add(sns[1]);
				while (!list.isEmpty()) {
					sns = list.removeFirst().split();
					neu.add(sns[0]);
					neu.add(sns[1]);
				}
				list = neu;
			}
			while (!list.isEmpty())
				System.out.println(list.removeFirst());
		}

	}

}
