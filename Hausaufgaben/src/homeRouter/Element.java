package homeRouter;


public class Element {

	private Element next;
	private Netzwerk value;
	private Route rt;
	
	public Element(Route rt){
		this.rt=rt;
	}
	
	public Element(Netzwerk value) {
		this.value = value;
	}

	public Element getNext() {
		return next;
	}

	public void setNext(Element next) {
		this.next = next;
	}
	
	public Route getRt(){
		return rt;
	}
	
	public Netzwerk getValue() {
		return value;
	}
}
