package homeDebug;

public class Auto {
	
	private Reifen reifen;
	private String farbe;
	private int nr;
	
	public void setReifen(Reifen reifen){
		this.reifen = reifen;
	}
	
	public Reifen getReifen(){
		return reifen;
	}
	
	public String getFarbe() {
		return farbe;
	}
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	public int getNr() {
		return nr;
	}
	public void setNr(int nr) {
		this.nr = nr;
	}
	
	
	
}
