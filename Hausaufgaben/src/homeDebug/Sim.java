package homeDebug;

public class Sim {

	public static void main(String[] args) {
		//mit Klasse Liste
		Liste l = new Liste();
		l.add("Hallo ");
		l.add("ich ");
		l.add("bin ");
		l.add("der ");
		l.add("Test!");
		System.out.println(l.size()); 
		System.out.println(l.toString()); //original
		System.out.println(l.toString()); //geaendert
		
		
		//mit Klasse Auto
		Auto a = new Auto();
		a.setFarbe("rot");
		a.setNr(3);
		a.setReifen(new Reifen(195));
		System.out.println(a.getFarbe());
		System.out.println(a.getNr());
		System.out.println(a.getReifen().getGroesse());
		
		//for Schleife
		int[] ia = {0,23,55,16,1337,74,2,3,6,1};
		System.out.println(ia.length);	
		for (int i = 0; i < ia.length; i++) 
			System.out.println(ia[i]);
		String []sa = {"Hans","Peter","Henry","Johannes"};
		for (int i=0; i < sa.length;i++) 
			System.out.println(sa[i]);
		//Aufruf log falsche Fakultät
		System.out.println(fak(5));
	}
	
	public static int fak(int x){ //Conditional BP x==1
		if(x==0) //log Bug Abbruchbedingung (x==0||x==1) 
			return x; //1
		else
			return x * fak(x-1);
	}
}
