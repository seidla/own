package homeDebug;

public class Liste {
	private Element first;

	public boolean isEmpty(){
		return first==null;
	}
	
	public void add(String value){
		if(first==null)
			first = new Element(value);
		else{
			Element tmp = new Element(value);
			//logischer Fehler first.next = first;
			tmp.next = first;
			first = tmp;
		}
	}
	
	public String toString(){
		String ret = "";
		Element tmp = first;
		for(;tmp != null;tmp=tmp.next)
				ret+=tmp.value;
		return ret;
	}
	
	public int size(){
		int ret = 0;
		Element tmp = first;
		for(;tmp!=null;tmp=tmp.next)
			ret++;
		return ret;
		
	}
	
	
	private class Element{
		private String value;
		private Element next;
		
		public Element(String value){
			this.value=value;
		}
			
	}
}
