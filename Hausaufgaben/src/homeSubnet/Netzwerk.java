package homeSubnet;

public class Netzwerk {
	private IpAdresse ip; // IP Adresse
	private IpAdresse bc; // broadcast
	private IpAdresse netIP; // netzip
	private IpAdresse mask; // netmask
	private char klasse;// A,B,C ...
	private int mc; // maxClients

	// constructors
	public Netzwerk(IpAdresse ip) {
		if (ip.get(0) < 128) {
			klasse = 'A';
			mask = new IpAdresse(255, 0, 0, 0);
		} else if (ip.get(0) < 192 && ip.get(0) > 127) {
			klasse = 'B';
			mask = new IpAdresse(255, 255, 0, 0);
		} else if (ip.get(0) < 224 && ip.get(0) > 191) {
			klasse = 'C';
			mask = new IpAdresse(255, 255, 255, 0);
		} else if (ip.get(0) < 240 && ip.get(0) > 223) {
			klasse = 'D';
			mask = new IpAdresse(255, 255, 255, 0);
		} else if (ip.get(0) < 256 && ip.get(0) > 239) {
			klasse = 'E';
			mask = new IpAdresse(255, 255, 255, 0);
		} else
			System.out.println("Klasse existiert nicht, Netz falsch");
		this.ip = ip;
		netIP = new IpAdresse(ip.toInt() & mask.toInt());
		bc = new IpAdresse((~mask.toInt()) | netIP.toInt());
		mc = ~mask.toInt() - 1;
	}

	public Netzwerk(IpAdresse ip, IpAdresse mask) {
		this.ip = ip;
		this.mask = mask;
		netIP = new IpAdresse(ip.toInt() & mask.toInt());
		bc = new IpAdresse((~mask.toInt()) | netIP.toInt());
		mc = ~mask.toInt() - 1;
	}

	// setters
	public void setMask(IpAdresse mask) {
		this.mask = mask;
	}

	// getters
	public IpAdresse getBc() {
		return bc;
	}

	public IpAdresse getNetIP() {
		return netIP;
	}

	public IpAdresse getMask() {
		return mask;
	}

	public int getMc() {
		return mc;
	}
	//Ausgabe Standard
	public String toStringSt() {
		return "Netzwerk [ip=" + ip.toString() + ", Klasse=" + klasse
				+ ", mask=" + mask.toString()+"]";
	}
	//Ausgabe Subnet
	public String toString(boolean a) {
		if(a)
			return "Netzwerk [NetzID=" + netIP.toString() + ", bc=" + bc.toString()+ ", mask=" + mask.toString()+ ", mC=" + mc + "]";
		else
			return "Netzwerk [NetzID=" + netIP.toString() + ", bc=" + bc.toString()+ ", mC=" + mc + "]";
	}
	public Netzwerk[] subnet(int clients, int sub){
		//var für MagicNbr
		int sBits=0;
		//wenn nach Anz Clients gesubnedddedd wird
		if(clients!=0)
			//Anz der führenden nullen von eingegebenen clients+1 + nachstellende nullen der maske -32 
			//(Bsp 60+1=26 nullen + /24 = 8 nullen --> 26+8-32 = 2 --> 4 Netze mit je 62 Clients
			sBits = Integer.numberOfLeadingZeros(clients+1)+Integer.numberOfTrailingZeros(this.getMask().toInt())-32;
		else
			//Bsp 4-1Netze = 30 führende nullen 32-30=2 --> 4 Netze
			sBits = 32-Integer.numberOfLeadingZeros(sub-1);
		//Array für die neuen netze mit der länge 2 hoch Anz der bits
		Netzwerk[] netze = new Netzwerk[(int)Math.pow(2, sBits)];
		//Maske neu schreiben
		IpAdresse neuMask = new IpAdresse(mask.toInt()>> sBits);
		//neues netz mit neuer Maske Bsp 192.168.0.0 /26
		Netzwerk tmp = new Netzwerk(getNetIP(),neuMask);
		//MaxClients in int speichern um schrittweite der neuen netze zu speichern Bsp 62mCl --> Schrittweite 64
		int maxCl = tmp.getMc()+2;
		//NetzID als int speichern um mit später mit schrittweite zu addieren
		int netIP = getNetIP().toInt();
		//in netze[] jedes mögl Netz schreiben und Schrittweite addieren
		for (int i = 0; i < netze.length; i++) {
			netze[i] = new Netzwerk(new IpAdresse(netIP),neuMask);
			netIP+=maxCl;
		}
		return netze;

}
}


