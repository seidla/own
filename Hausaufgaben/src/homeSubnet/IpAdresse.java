package homeSubnet;

public class IpAdresse {
	int adresse;

	public IpAdresse(int a) {
		this.adresse = a;
	}

	public IpAdresse(int a, int b, int c, int d) {
		this.adresse = (a * (int) Math.pow(256, 3))
				+ (b * (int) Math.pow(256, 2)) + (c * (int) Math.pow(256, 1))
				+ d;
	}

	public IpAdresse(int[] a) {
		this.adresse = (a[0] * (int) Math.pow(256, 3))
				+ (a[1] * (int) Math.pow(256, 2))
				+ (a[2] * (int) Math.pow(256, 1)) + a[3];
	}

	public int get(int a) {
		switch (a) {
		case 0:
			return (adresse >> 24) & 255;
		case 1:
			return (adresse >> 16) & 255;
		case 2:
			return (adresse >> 8) & 255;
		case 3:
			return adresse & 255;
		default:
			return -1;
		}
	}

	public void set(int index, int oktett) {
		switch (index) {
		case 0:
			adresse = (adresse & 16777215) + (oktett * (int) Math.pow(256, 3));
			break;
		case 1:
			adresse = (adresse & -16711681) + (oktett * (int) Math.pow(256, 2));
			break;
		case 2:
			adresse = (adresse & -65281) + (oktett * (int) Math.pow(256, 1));
			break;
		case 3:
			adresse = (adresse & 256) + oktett;
			break;
		default:
			break;
		}
	}

	public int toInt() {
		int tmp = 0;
		for (int i = 0, j = 3; i < 4; i++, j--)
			tmp += (get(i) * (int) Math.pow(256, j));
		return tmp;
	}

	public String toString() {
		return get(0)+"."+get(1)+"."+get(2)+"."+get(3);
	}
	
}


