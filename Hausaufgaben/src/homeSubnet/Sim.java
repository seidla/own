package homeSubnet;
import java.util.Scanner;
public class Sim {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		// Aufgabe 1-----------------------------------------------------------
		 System.out.print("Gib die IP ein: ");
		 //Eingabe bei . splitten --> 192 . 168 etc --> in Str[] 
		 String[] ipIn = s.next().split("\\.");
		 //Inhalt ipIn in int[] umwandeln
		 int[] ips = new int[4];
		 for (int i = 0; i < 4; i++)
		 ips[i] = Integer.parseInt(ipIn[i]);
		 //in IpAdresse umwandeln
		 IpAdresse ip = new IpAdresse(ips);
		 //Netzwerk erzeugen mit eingegebener Ip und defMask
		 Netzwerk aufg1 = new Netzwerk(ip);
		 //Ausgabe wie in Aufg1 gefordert (s. Netzwerk.toStringSt())
		 System.out.println(aufg1.toStringSt());
		 //-------------------------------------------------------------------
		 System.out.println();
		 //Aufgabe 2-----------------------------------------------------------
		 System.out.print("Gib die Maske ein: ");
		 ipIn = s.next().split("\\.");
		 ips = new int[4];
		 for (int i = 0; i < 4; i++)
		 ips[i] = Integer.parseInt(ipIn[i]);
		 //Maske in "ip" umwandeln
		 IpAdresse mask = new IpAdresse(ips);
		 //Nw mit neuer maske erzeugen
		 Netzwerk aufg2 = new Netzwerk(ip,mask);
		 //Ausgabe wie in Aufg2 gefordert (s. Netzwerk.toString(bool)
		 System.out.println(aufg2.toString(true));
		// --------------------------------------------------------------------
		System.out.println();
		// Aufgabe 3-----------------------------------------------------------
		//Vars für Anzahl des gewählten subnettings
		int subnet = 0;
		int clients = 0;
		System.out.println("Subnetting");
		System.out.println("----------");
		System.out.println("[1] Anzahl Subnetze eingeben");
		System.out.println("[2] Anzahl Clients eingeben");
		//Leider true
		System.out.println("[0] Ich bin an der FSBwIT ich muss das per Hand machen!");

		System.out.print("Auswahl: ");
		int auswahl = s.nextInt();
		if (auswahl == 1) {
			System.out.println("Subnetze: ");
			subnet = s.nextInt();
		}else if (auswahl == 2) {
			System.out.println("Clients: ");
			clients = s.nextInt();
		}else return;
		//Nw[] für aufg3 erzeugen
		Netzwerk[] aufg3 = new Netzwerk[0];
		//var für gesamte Clients
		int allCl=0;
		//subnetting und [] zeiger verschieben
		aufg3 = aufg1.subnet(clients, subnet);
		for (int i = 0; i < aufg3.length; i++) {
			System.out.print(i+1+". ");
			System.out.println(aufg3[i].toString(false));
			allCl+=aufg3[i].getMc();
		}
		System.out.println();
		System.out.println(aufg3.length + " Netze, mit insgesamt "+ allCl+" Clients");
	}
}
