package home141103strings;

public class Version2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String orgText = "Hallo, ich bin der fest vorgegebene Text."; // originaler
																		// Text
		String revText = ""; // var für den umgestellten Text!
		int tmp = 0; // var um Stelle der gefundenen Zeichen zu speichern
		for (int i = 0; i <= orgText.length() - 1; i++) { // gehe durch den
															// kompletten Text
															// von index 0 bis
															// letzten index
			if (orgText.charAt(i) == ' ' | orgText.charAt(i) == ','
					| orgText.charAt(i) == '.' | orgText.charAt(i) == '!'
					| orgText.charAt(i) == '?') { // prüfe jeden "Char" im
													// String auf bestimmte
													// Zeichen
				for (int x = i - 1; x >= tmp; x--) { // Wenn bestimmtes Zeichen
														// gefunden -> springe
														// an Stelle vor dem
														// bestimmten Zeichen(4)
														// und gehe zum
														// anfang (0)
														// und schiebe die
														// "chars" von letzter
														// Stelle (4) an 1.
														// Stelle(0)
					if (Character.isUpperCase(orgText.charAt(x)) == true) {
					}
					revText = revText + orgText.charAt(x); // "" = "" + "char"
															// an
															// der Stelle x (4)
															// (3)
															// (2) (1) (0) ->
															// ollaH
				}
				tmp = revText.length() + 1; // setze den Anfang auf die Anzahl
											// der Chars welche schon gedreht
											// wurden "ollaH" = 4 (+1) Weil an
											// Stelle 5 im original String
											// weiter gemacht wird
				revText = revText + orgText.charAt(i); // + gefundene Zeichen
														// -->
														// ollaH + ","+ " "
			} else if (i == orgText.length() - 1) { // Wenn das Ende des Strings
													// erreicht ist (und kein
													// Zeichen steht)
													// =".... Text"
				for (int a = i; a >= tmp; a--) { // zähle von letztem "Char" im
													// String bis zur Anzahl der
													// bereits gedrehten
													// Chars+Zeichen
					revText = revText + orgText.charAt(a); // "...enebegegrov "
															// +
															// txeT
				}
			}
		}
		System.out.println(orgText+ " <---> " +revText); 
	}
}