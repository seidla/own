package home141103strings;

public class Version3 {
	public static void main(String[] args) {
		// ---------------- mit Scanner ----------------------------------------
//		 java.util.Scanner s = new java.util.Scanner(System.in);
//		 String orgText = s.nextLine();
		// ---------------------------------------------------------------------
		// String orgText = "Hallo, ich bin der fest vorgegebene Text.";
		String orgText = "Fallschirmjäger sein, ist der Protest gegen das Gesetz des Durchschnitts!"; // originaler
																								// Text!
		String revText = ""; // var für den umgestellten Text!
		char[] revZeichen = new char[orgText.length()]; // array um revText in
														// einzelne Zeichen zu
														// zerlegen
		int[] grStelle = new int[orgText.length()]; // array um Stellen
													// Großbuchstaben im orgText
													// zu merken
		String finText = ""; // var um den finalen Text zusammen zu setzen
		int tmp = 0; // var um Stelle der gefundenen Zeichen zu speichern
		for (int i = 0; i <= orgText.length() - 1; i++) { // gehe durch den
															// kompletten Text
															// von index 0 bis
															// letzten index
			if (orgText.charAt(i) == ' ' | orgText.charAt(i) == ','
					| orgText.charAt(i) == '.' | orgText.charAt(i) == '!'
					| orgText.charAt(i) == '?') { // prüfe jeden "Char" im
													// String auf bestimmte
													// Zeichen
				for (int x = i - 1; x >= tmp; x--) { // Wenn bestimmtes Zeichen
														// gefunden -> springe
														// an Stelle vor dem
														// bestimmten Zeichen(4)
														// und gehe zum
														// anfang (0)
														// und schiebe die
														// "chars" von letzter
														// Stelle (4) an 1.
														// Stelle(0)
					if (Character.isUpperCase(orgText.charAt(x)) == true) {
					}
					revText = revText + orgText.charAt(x); // "" = "" + "char"
															// an
															// der Stelle x (4)
															// (3)
															// (2) (1) (0) ->
															// ollaH
				}
				tmp = revText.length() + 1; // setze den Anfang auf die Anzahl
											// der Chars welche schon gedreht
											// wurden "ollaH" = 4 (+1) Weil an
											// Stelle 5 im original String
											// weiter gemacht wird
				revText = revText + orgText.charAt(i); // + gefundene Zeichen
														// -->
														// ollaH + ","+ " "
			} else if (i == orgText.length() - 1) { // Wenn das Ende des Strings
													// erreicht ist (und kein
													// Zeichen steht)
													// =".... Text"
				for (int a = i; a >= tmp; a--) { // zähle von letztem "Char" im
													// String bis zur Anzahl der
													// bereits gedrehten
													// Chars+Zeichen
					revText = revText + orgText.charAt(a); // "...enebegegrov "
															// +
															// txeT
				}
			}
		}
		revText = revText.toLowerCase();
		for (int a = 0; a <= orgText.length() - 1; a++) { //
			if (Character.isUpperCase(orgText.charAt(a)) == true) { // wenn ein
																	// Buchstabe
																	// großgeschrieben
																	// ist im
																	// orgText
				grStelle[a] = 1; // Setze eine 1 an der Stelle des orgTextes wo
									// ein Großbuchstabe steht Bsp "Hallo" -->
									// an
									// index 0 = 1!
			}
			revZeichen[a] = revText.charAt(a); // Speichere jeden "Char" des
												// revTextes in ein Feld des
												// Arrays
			if (grStelle[a] == 1) { // wenn Stelle vom Großbuchstaben im orgText
									// gefunden
				revZeichen[a] = Character.toUpperCase(revZeichen[a]); // an der
																		// Stelle
																		// wo
																		// orgText=
																		// Großbuchstabe
																		// mache
																		// aus
																		// dem
																		// jeweiligen
																		// "Chars"
																		// im
																		// Feld
																		// des
																		// Arrays
																		// einen
																		// Großbuchstaben
			}
			finText += revZeichen[a]; // setze von 0 bis ende des Arrays alle
										// "Chars" wieder zusammen in den String
										// finText
		}
		System.out.println(orgText + " <---> " + finText);
	}
}
