package homeRouterV2;

public interface Iterator {
	public boolean hasNext();
	public Route next();
}
