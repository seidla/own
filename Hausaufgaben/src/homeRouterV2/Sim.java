package homeRouterV2;

import java.util.Scanner;

public class Sim {

	public static void main(String[] args) {
		System.out.println("Router");
		System.out.println("------");
		Router rTab = new Router();
		boolean check = true;
		Scanner s = new Scanner(System.in);
		String in="";
		while (check) {
			in = s.nextLine();
			String[] inArr = in.split("\\,");
			if (inArr.length == 4) {
				rTab.add(new Route(new Netzwerk(new IPAdresse(inArr[0]),
						new IPAdresse(inArr[1])), new IPAdresse(inArr[2]),
						Integer.parseInt(inArr[3])));}
			else if(!rTab.isEmpty()&&checkIP(in))
				check = false;
			else if(rTab.isEmpty()&&checkIP(in))
				System.out.println("Fehler, keine Routen verfügbar!");
			else
				System.out.println("Fehler, keine gültige Route oder DstIP");
		}
		
		while (!(in.equalsIgnoreCase("Ende"))) {
			if (checkIP(in))
				System.out.println(rTab.bestMatch(new IPAdresse(in)));
			else
				System.out.println("Fehler!");
			in=s.nextLine();
		}
	}

	public static boolean checkIP(String in){
		int count = 0;
		String[] inArr = in.split("\\.");
		if (inArr.length==4)
			for(String s : inArr)
				if(s.matches("[0-9]+")&&Integer.parseInt(s)<=255) count++;
		
		return count==4;									
		
	}
}
