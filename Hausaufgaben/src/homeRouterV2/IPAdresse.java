package homeRouterV2;


public class IPAdresse {

	private int ip;
	
	public IPAdresse(int ip) {
		this.ip = ip;
	}
	
	public IPAdresse(int o1, int o2, int o3, int o4) {
		ip = (((((o1 <<8) | o2) <<8) | o3) <<8) | o4; 
	}
	
	public IPAdresse(int[] os) {
		ip = (((((os[0] <<8) | os[1]) <<8) | os[2]) <<8) | os[3];
	}
	
	//Router
	public IPAdresse(String ip){
		String[] ipo = ip.split("\\.");
		this.ip= (((((Integer.parseInt(ipo[0]) <<8) | Integer.parseInt(ipo[1])) <<8) | Integer.parseInt(ipo[2])) <<8) | Integer.parseInt(ipo[3]);
	}
	
	public int get(int no) {
		return (ip >> ((4-no)*8)) & 255;
	}
	
	public void set(int no, int o) {
		int maske = 255 << ((4-no)*8);
		ip = (o << ((4-no)*8)) & maske | (ip & (~maske));
	}
	
	public int toInt() {
		return ip;
	}
	
	public String toString() {
		return get(1)+"."+get(2)+"."+get(3)+"."+get(4);
	}
}
