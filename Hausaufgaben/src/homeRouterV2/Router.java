package homeRouterV2;

public class Router {
	List rTab = new List();

	public boolean isEmpty(){
		return rTab.isEmpty();
	}
	public void add(Route rt) {
		rTab.add(rt);
	}

	public String bestMatch(IPAdresse ip) {
		Route best = rTab.first.val;
		for (Iterator a = rTab.new Count(); a.hasNext();) {
			Route tmp = a.next();
			if (tmp.lpm(ip) > best.lpm(ip))
				best = tmp;
		}
		if (best.lpm(ip) < 0)
			return "Keine Route gefunden!";
		else
			return "über " + best.getIface() + " an " + best.getGw();
	}

	public String toString() {
		String out = "";
		System.out.println("NetzwerkIP" + '\t' + "Subnetzmaske" + '\t'
				+ "Gateway" + "         " + "Interface");
		for (Iterator a = rTab.new Count(); a.hasNext();)
			out += a.next().toString() + '\n';
		return out;
	}

	private class List {
		private E first;

		public boolean isEmpty() {
			return first == null;
		}
		/*
		public int size() {
			int i = 0;
			for (E tmp = first; tmp != null; tmp = tmp.next, i++)
				;
			return i;
		}
		*/
		public void add(Route o) {
			if (first == null)
				first = new E(o);
			else {
				E tmp = new E(o);
				tmp.next = first;
				first=tmp;
			}
		}

		public class Count implements Iterator {
			private E current = first;

			public boolean hasNext() {
				return current != null;
			}

			public Route next() {
				Route ret = current.val;
				current = current.next;
				return ret;
			}
		}

		private class E {
			private E next;
			private Route val;

			public E(Route val) {
				this.val = val;
			}
		}
	}

}
