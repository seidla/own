package homeStack;

public class CharStack {
	private char[] chStack = new char[0];

	public void push(char z) {
		char[] tmp = new char[chStack.length+1];
		
		for(int i=0;i<chStack.length;i++)
			tmp[i] = chStack[i];
		tmp[tmp.length-1] = z;
		chStack = tmp;
				
	}

	public char pop() {
		char[] tmp = new char[chStack.length-1];
		char z = chStack[chStack.length-1];
		for (int i = 0;i<tmp.length;i++)
			tmp[i] = chStack[i];
		chStack = tmp;
		return z;
	}

	public boolean isEmpty() {
		return chStack.length == 0;
	}
}
