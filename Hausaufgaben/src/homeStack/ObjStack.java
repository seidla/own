package homeStack;

public class ObjStack {
	Object[] objStack = new Object[0];
	public void push(Object z) {
		Object[] tmp = new Object[objStack.length+1];
		
		for(int i=0;i<objStack.length;i++)
			tmp[i] = objStack[i];
		tmp[tmp.length-1] = z;
		objStack = tmp;
				
	}

	public Object pop() {
		Object[] tmp = new Object[objStack.length-1];
		Object z = objStack[objStack.length-1];
		for (int i = 0;i<tmp.length;i++)
			tmp[i] = objStack[i];
		objStack = tmp;
		return z;
	}

	public boolean isEmpty() {
		return objStack.length==0;
		
	}
}
