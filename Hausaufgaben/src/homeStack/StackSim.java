package homeStack;

public class StackSim {

	public static void main(String[] args) {
		// int Stack
		Stack intStack = new Stack();
		intStack.push(1);
		intStack.push(2);
		intStack.push(3);
		while (!intStack.isEmpty())
			System.out.println(intStack.pop());

		// char Stack mit chars
		CharStack charStack = new CharStack();
		charStack.push('x');
		charStack.push('y');
		charStack.push('z');
		while (!charStack.isEmpty())
			System.out.println(charStack.pop());

		// char Stack mit String
		CharStackStr strStack = new CharStackStr();
		strStack.push('a');
		strStack.push('b');
		strStack.push('c');
		while (!strStack.isEmpty())
			System.out.println(strStack.pop());

		// Object Stack
		ObjStack objStack = new ObjStack();
		String o1 = "eins";
		String o2 = "zwo";
		String o3 = "drei";
		objStack.push(o1);
		objStack.push(o2);
		objStack.push(o3);
		while (!objStack.isEmpty())
			System.out.println(objStack.pop());
	}

}
