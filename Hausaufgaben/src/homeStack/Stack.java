package homeStack;

public class Stack {
	private int[] intStack = new int[0];

	public void push(int z) {
		int[] tmp = new int[intStack.length + 1];

		for (int i = 0; i < intStack.length; i++)
			tmp[i] = intStack[i];
		tmp[tmp.length - 1] = z;
		intStack = tmp;

	}

	public int pop() {
		int[] tmp = new int[intStack.length - 1];
		int z = intStack[intStack.length - 1];
		for (int i = 0; i < tmp.length; i++)
			tmp[i] = intStack[i];
		intStack = tmp;
		return z;
	}

	public boolean isEmpty() {
		return intStack.length == 0;
	}

}
